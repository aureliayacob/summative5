# summative5

1. Landing Page

![1-LandingPage](./screenshots/1-LandingPage.jpg)



2.After Login Page

![2-AfterLoginPage](./screenshots/2-AfterLoginPage.jpg)



3 LoginPage

![3-LoginPage](./screenshots/3-LoginPage.jpg)

4.Detail View

![4-QuickView](./screenshots/4-QuickView.jpg)

5.After Clicking Logout

![6-AfterClickingLogout](./screenshots/6-AfterClickingLogout.jpg)

6.Dashboard

![7-Dashboard](./screenshots/7-Dashboard.jpg)
