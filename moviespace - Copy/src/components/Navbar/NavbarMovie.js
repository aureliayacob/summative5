import 'bootstrap/dist/css/bootstrap.min.css';
import './Navbar.css'
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { moviesActions } from '../../store/movies';
import { Link } from 'react-router-dom';
import auth, { authActions } from '../../store/auth';

function NavbarMovie() {
    const listGenre = useSelector((state) => state.movies.listGenre);
    const dispatch = useDispatch();
    let buttonValue = " Log in"

    useEffect(() => {
        //list Genres
        async function getListGenreAPI() {
            let response = await fetch(
                "https://api.themoviedb.org/3/genre/movie/list?api_key=875eb1ac14bc7a035e774c62792fc3f4&language=en-US"
            );
            let result = await response.json();
            return result;
        }

        getListGenreAPI()
            .then((data) => {
                dispatch(moviesActions.getListOfGenres(data.genres));

            })
            .catch((err) => {
                alert("FETCHING ERROR");
            });
    }, [dispatch]);
    console.log(listGenre);

    const currentUser = useSelector((state) => {
        return state.auth;
    });

    if (currentUser.isAuth === true) {
        buttonValue = "Log out";
    }

    function logoutHandler() {
        if (currentUser.isAuth === true) {
            dispatch(authActions.userLogout(false));
            buttonValue = "Log in";
        }
    }
    console.log(currentUser.user)
    return (
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container-fluid">
                <Link to="/LandingPage">
                    <button className="custom-btn-nav">  <h6 class="navbar-brand"><i class="fa fa-film" /> MOVIE SPACE</h6></button>
                </Link>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <Link to="/">
                                <button className="custom-btn-nav">Home</button>
                            </Link>
                        </li>
                        <li class="nav-item">
                            <Link to="/dashboard">
                                <button className="custom-btn-nav">Dashboard</button>
                            </Link>
                        </li>
                    </ul>

                    <form class="d-flex">
                        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                        <button class="btn" type="submit">Search</button>
                    </form>
                    <span class="navbar-text">
                        <Link to="/login">
                            <button className="custom-btn-nav" onClick={logoutHandler}><i class="fas fa-user-alt" /><b> {buttonValue}</b></button>
                        </Link>
                    </span>
                </div>
            </div >
        </nav >
    );
}
export default NavbarMovie;