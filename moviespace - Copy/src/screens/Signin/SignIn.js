import "./SignIn.css"
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from "react-router-dom";
import { authActions } from "../../store/auth"
import { useSelector, useDispatch } from "react-redux";
import { useState } from "react";
import { useNavigate } from "react-router";

function Signin() {
    let redir;
    const dispatch = useDispatch();
    let navigate = useNavigate();
    const auth = useSelector((state) => state.auth.isAuth);
    console.log("AUTH " + auth);
    const [user, setUser] = useState({
        username: "",
        password: "",
    });
    const formHandler = (events) => {
        return setUser({
            ...user,
            [events.target.name]: events.target.value,
        });
    };

    const userHandler = (events) => {
        events.preventDefault();
        console.log(user);
        dispatch(authActions.validateUser(user));
        if (auth === true) {
            navigate("/dashboard");
        }
    }



    return (
        <div class="container-fluid">
            <div className="custom-container">
                <form onSubmit={userHandler}>
                    <div class="mb-3">
                        <label className="custom-title"><h5>LOGIN</h5></label>
                    </div>
                    <div class="mb-3">
                        <label htmlFor="username">Username</label>
                        <input type="text" name="username" onChange={formHandler} />
                    </div>
                    <div class="mb-3">
                        <label htmlFor="password">Password</label>
                        <input type="password" name="password" onChange={formHandler} />
                    </div>


                    <button type="submit" className="custom-btn">Submit</button>

                </form>
            </div>
        </div >

    );
}
export default Signin;