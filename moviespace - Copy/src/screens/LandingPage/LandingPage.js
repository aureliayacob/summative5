import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { moviesActions } from "../../store/movies"
import OurCarousel from "../Carousel/Carousel";
import Dashboard from "../Dashboard/Dashboard";

function LandingPage() {
    const listMovies = useSelector((state) => state.movies.listMovies);

    return (
        <div>
            <OurCarousel />
            <Dashboard />
        </div>
    );

}
export default LandingPage;