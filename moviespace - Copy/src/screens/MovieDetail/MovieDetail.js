import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { moviesActions } from "../../store/movies";
import { useParams } from "react-router";
import "./MovieDetail.css"

function MovieDetail() {
    let param = useParams();
    const listVideo = useSelector((state) => state.movies.listVideo);
    const listMovies = useSelector((state) => state.movies.listMovies);
    const dispatch = useDispatch();

    useEffect(() => {
        //detail
        async function getMoviesByIdAPI() {
            let response = await fetch(
                `https://api.themoviedb.org/3/movie/${param.id}?api_key=875eb1ac14bc7a035e774c62792fc3f4&language=en-US`
            );
            let result = await response.json();
            return result;
        }

        getMoviesByIdAPI()
            .then((data) => {
                dispatch(moviesActions.getAllMovies(data));
            })
            .catch((err) => {
                alert("ERROR");
            });

        async function getMoviesVideoIdAPI() {
            let response = await fetch(
                `https://api.themoviedb.org/3/movie/${param.id}/videos?api_key=875eb1ac14bc7a035e774c62792fc3f4&language=en-US`
            );
            let result = await response.json();
            return result;
        }

        getMoviesVideoIdAPI()
            .then((data) => {
                dispatch(moviesActions.getVideo(data.results));
            })
            .catch((err) => {
                alert("ERROR");
            });
    }, [dispatch]);

    // const year = listMovies.release_date.split("-", 3);
    console.log(listVideo);
    // console.log(listMovies);

    return (
        <div class="flex-container">

            <div>
                <div class="card-img">
                    <img src={`http://image.tmdb.org/t/p/w500${listMovies.poster_path}`} class="img-detail" alt="..." />

                </div>
            </div>
            <div>
                <div class="card">
                    <div class="card-body">
                        <h5 class="detail-title">{listMovies.title}  </h5>
                        <div class="content-title">Overview : </div>
                        <div class="content-desc">{listMovies.overview}</div>
                        <br />
                        <div class="content-title"> Genres :  </div>
                        <div class="content-desc">

                            <ul>

                                {
                                    listMovies.genres.map((value, index) => {
                                        return (
                                            <li> {value.name}, </li>
                                        );
                                    })

                                }
                            </ul>
                        </div>
                        <iframe width="420" height="315"
                            src={`https://www.youtube.com/embed/${listVideo[0].key}`}
                            frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div >
    );

}
export default MovieDetail;