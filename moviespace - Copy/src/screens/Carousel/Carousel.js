import 'bootstrap/dist/css/bootstrap.min.css';
import './Carousel.css'
import '../Dashboard/Dashboard.css'
import { useEffect } from 'react';
import { Carousel, Container } from "react-bootstrap";
import { useDispatch, useSelector } from 'react-redux';
import { moviesActions } from '../../store/movies';


const OurCarousel = () => {
    // const listVide = useSelector((state)=>state)
    const listNoWPlaying = useSelector((state) => state.movies.listPlaying);
    const dispatch = useDispatch();

    useEffect(() => {
        //nowPlaying
        async function getNowPlayingAPI() {
            let response = await fetch(
                "https://api.themoviedb.org/3/movie/now_playing?api_key=875eb1ac14bc7a035e774c62792fc3f4&language=en-US&page=1"
            );
            let result = await response.json();
            return result;
        }

        getNowPlayingAPI()
            .then((data) => {
                dispatch(moviesActions.getNowPlaying(data.results));

            })
            .catch((err) => {
                alert("FETCHING ERROR");
            });
    }, [dispatch]);

    console.log("NOW" + listNoWPlaying);
    return (
        <div>
            <Container className="container-fluid">
                <h3 className="title-page">NOW PLAYING</h3>
                <div class="container py-3">

                    <Carousel className="carousel">
                        {/* <Carousel.Item>
                            <img className="d-block w-100" src={`http://image.tmdb.org/t/p/w500/eeijXm3553xvuFbkPFkDG6CLCbQ.jpg`} alt="First slide" />
                        </Carousel.Item> */}
                        {
                            listNoWPlaying.map((value, index) => {
                                return (
                                    // <div >
                                    // {console.log(index)}
                                    <Carousel.Item key={index}>
                                        <div className="trim" >
                                            <img className="d-block w-100" src={`http://image.tmdb.org/t/p/w500${value.backdrop_path}`} alt="First slide" />
                                        </div>
                                        <Carousel.Caption>
                                            <h3>{value.title}</h3>
                                            <p>{value.overview}</p>
                                        </Carousel.Caption>
                                    </Carousel.Item>
                                    // </div>
                                );
                            })
                        }
                    </Carousel>
                </div>
            </Container>
        </div>
    );
}

export default OurCarousel;
