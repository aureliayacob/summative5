import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { moviesActions } from "../../store/movies"
import { Link } from "react-router-dom";
import "./Dashboard.css"


function Dashboard() {
    const listMovies = useSelector((state) => state.movies.listMovies);
    const dispatch = useDispatch();

    useEffect(() => {
        //trending
        async function getMoviesAPI() {
            let response = await fetch(
                "https://api.themoviedb.org/3/trending/movie/week?api_key=875eb1ac14bc7a035e774c62792fc3f4"
            );
            let result = await response.json();
            return result;
        }

        getMoviesAPI()
            .then((data) => {
                dispatch(moviesActions.getAllMovies(data.results));

            })
            .catch((err) => {
                alert("KALO TERJADI ERROR");
            });
    }, [dispatch]);

    function DetailHandler(id) {
        dispatch(moviesActions.setIdToSEarch(id))

    }

    return (
        <div>
            <div class="container-fluid py-3"><h3 className="title-page">Trending</h3></div>
            <div class="flex-container">
                {
                    listMovies.map((value, index) => {

                        const linkto = "/movie/detail/" + value.id;
                        return (
                            <div key={index}>
                                <div class="card">
                                    <img src={`http://image.tmdb.org/t/p/w500${value.poster_path}`} class="card-img-top" alt="..." />
                                    {/* <div class="card-body"> */}
                                    <p class="card-title">{value.title}</p>
                                    <p class="card-text">{value.release_date}</p>
                                    {/* <Link to={linkto}> */}
                                    <Link to={linkto}>
                                        <button type="button" className="quick-view-btn" ><i class="far fa-eye" /><b> Quick View</b></button>
                                    </Link>
                                </div>
                            </div>);
                    })
                }

            </div>
        </div >

    );

}
export default Dashboard;