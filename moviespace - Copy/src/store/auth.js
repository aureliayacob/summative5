import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    user: "",
    isAuth: false,
};

const loginSlices = createSlice({
    name: "auth",
    initialState: initialState,
    reducers: {
        validateUser(state, data) {
            if (data.payload.username === "aurel") {
                state.isAuth = true;
                console.log("STATE " + state.isAuth);
                state.user = data.payload.username;

            } else {
                alert("LOGIN GAGAL")
            }
        },
        userLogout(state, data) {
            state.isAuth = data;
        }
    }
});

export const authActions = loginSlices.actions;

export default loginSlices.reducer;
