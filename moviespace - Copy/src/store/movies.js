import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    searchById: 0,
    listMovies: [],
    listPlaying: [],
    listGenre: [],
    listVideo: [],
};

const moviesSlices = createSlice({
    name: "movies",
    initialState: initialState,
    reducers: {
        getAllMovies(state, data) {
            state.listMovies = data.payload;
            alert("OK");
        },
        getNowPlaying(state, data) {
            state.listPlaying = data.payload;
        },
        getListOfGenres(state, data) {
            state.listGenre = data.payload;
        },
        setIdToSEarch(state, data) {
            state.searchById = data.payload;
        },
        getVideo(state, data) {
            state.listVideo = data.payload;
        }
    },
});

export const moviesActions = moviesSlices.actions;

export default moviesSlices.reducer;
