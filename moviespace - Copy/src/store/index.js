import { configureStore } from "@reduxjs/toolkit";
import auth from "./auth";
import movies from "./movies";

const store = configureStore({
    reducer: {
        movies: movies,
        auth: auth,
    },
});

export default store;
