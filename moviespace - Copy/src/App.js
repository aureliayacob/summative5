import { Fragment } from 'react';
import { Route, Routes } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import LandingPage from './screens/LandingPage/LandingPage';
import Signin from './screens/Signin/SignIn';
import NavbarMovie from './components/Navbar/NavbarMovie';
import Dashboard from './screens/Dashboard/Dashboard';
import MovieDetail from './screens/MovieDetail/MovieDetail';

function App() {
  const isAuth = useSelector((state) => {

    return state.auth.isAuth;
  });
  console.log(isAuth)
  return (
    <Fragment>

      {/* {isAuth ? ( */}
      <>
        <NavbarMovie />
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="/login" element={<Signin />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/movie/detail/:id" element={<MovieDetail />} />
        </Routes>
      </>
      {/* ) : ( */}
      {/* <div> */}
      {/* <NavbarMovie /> */}
      {/* <Signin /> */}
      {/* <MovieDetail /> */}

      {/* </div> */}

      {/* )} */}
    </Fragment>
  );
}

export default App;
